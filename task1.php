<?php

class Student
{
    public $array_heights;
    public $array_surnames;
    public $height = 0;
    public $index = 0;

    public function __construct(array $array_heights, array $array_surnames)
    {
        $this->array_heights = $array_heights;
        $this->array_surnames = $array_surnames;
    }

    public function getSurname()
    {
        for ($i = 0; $i < count($this->array_heights); $i++) {
            if ($this->array_heights[$i] > $this->height) {
                $this->height = $this->array_heights[$i];
                $this->index = $i;
            }
        }
        return $this->array_surnames[$this->index];
    }
}

$array_heights = ['190','165', '175', '170', '180'];
$array_surnames = ["Atavaliev", "Adminov", "Petrov", "Ivanov", "Sidorov"];

$student = new Student($array_heights,$array_surnames);
$surname = $student->getSurname();
print "$surname \n";
